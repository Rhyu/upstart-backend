<h1>Upstart Onsite Services</h1>
<h3>Prerequisites:</h3>
<h5>Confirming Installations</h5>

```
#Java 12
➜ java --version
openjdk 12.0.1 2019-04-16   #Needs to be 12, Oracle or OpenJDK are fine.
OpenJDK Runtime Environment (build 12.0.1+12)
OpenJDK 64-Bit Server VM (build 12.0.1+12, mixed mode, sharing)
```

```
#Maven
➜ mvn --version
Apache Maven 3.6.0 (97c98ec64a1fdfee7767ce5ffb20918da4f719f3; 2018-10-24T14:41:47-04:00)
Maven home: /Users/*****/ext/apache-maven-3.6.0
Java version: 12.0.1, vendor: Oracle Corporation, runtime: /Library/Java/JavaVirtualMachines/jdk-12.0.1.jdk/Contents/Home
Default locale: en_US, platform encoding: UTF-8
OS name: "mac os x", version: "10.13.6", arch: "x86_64", family: "mac"
```
<h5>Requirement Downloads</h5>
<ul>
    <li><a href="https://www.oracle.com/technetwork/java/javase/downloads/jdk12-downloads-5295953.html">Java 12</a></li>
    <li><a href="https://maven.apache.org/download.cgi">Maven</a></li>
</ul>


<h3>How to Run:</h3>

```java
➜ mvn spring-boot:run
   or
➜ mvn package && java -jar target/{{artifactId}}-0.0.1-SNAPSHOT.jar
```

<h3>Package Structure</h3>

```
|
\java
 \com
  \aep
   \charge
    \{{artifactId}}
     \configuration 
      \spring
      -SecurityConfiguration.java   #Spring Security declaration, SSO and ant matcher declarations.
     \controller    #Controllers are the input handler classes for interfacing with your API.
      -BaseController.java  #Base RestController component for sharing functionality across all Controllers.
     \dao       #DAOs control data interactions from datasources, including mappers.
      -BaseDAO.java  #Base DAO Object for sharing functionality across all DAO's.
     \delegate  #Delegates control primary business functionality. Called from Controllers.
      -BaseDelegate.java #Base Delegate object for sharing functionality across all Delegates.
     \domain    #Collection of POJO's (Objects) for the project.
      \request
       -ServiceRequest.java  #Base POJO for all Service Request Objects
      \response
       -ServiceResponse.java  #Base POJO for all Service Response Objects
     \exception
      \exceptions    #Housing for any custom Application Exceptions that are thrown.
      -ApplicationControllerAdvice.java  #Default Controller Advice declaration. Instantiates Exception Handlers.
      -BaseExceptionHandler.java         #Base Exception Handler object for sharing functionality and implementation across all Exception Handlers.
      -GenericExceptionHandler.java      #Default Exception Handler.  If an unhandled Exception is thrown this handler catches it.
     \util
      -Constants.java    #Generic housing for any application constants that are required.
     -{{projectName}}Template.java  #Spring Boot Project Declaration
\resources
 \logging
  -log4j2-{env}.xml             #Log4j2 Configurations for each environment. Active Profile drives this from application-{env}.yml
 -application.yml               #Default Profile Application Properties for Spring Boot.
 -application-cloud.yml         #Shared application configuration overrides for Cloud Deployments.
 -application-{env}.yml #Environment specific configuration overrides.
```