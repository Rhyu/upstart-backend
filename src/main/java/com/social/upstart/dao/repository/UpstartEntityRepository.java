package com.social.upstart.dao.repository;

import com.social.upstart.domain.upstart.UpstartEntity;
import org.springframework.data.repository.CrudRepository;

public interface UpstartEntityRepository extends CrudRepository<UpstartEntity, Integer> {
}
