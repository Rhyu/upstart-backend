package com.social.upstart.controller;

import com.social.upstart.dao.repository.UpstartEntityRepository;
import com.social.upstart.domain.response.ServiceResponse;
import com.social.upstart.domain.response.upstart.UpstartEntityListResponse;
import com.social.upstart.domain.response.upstart.UpstartEntityResponse;
import com.social.upstart.domain.upstart.UpstartEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.QueryParam;
import java.util.Optional;

/**
 * Controller that handles the Versioning endpoint.  Can be utilized for a health check as well as returning some
 * instance metadata -- including but not limited to semantic version, and active spring-boot profiles.
 */
@RestController
@RequestMapping(path = "upstart")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UpstartController extends BaseController {
    private UpstartEntityRepository entityRepository;

    @Autowired
    public UpstartController(UpstartEntityRepository entityRepository) {
        super(UpstartController.class);
        this.entityRepository = entityRepository;
    }

    @PostMapping(path = "entity")
    public @ResponseBody
    ServiceResponse addEntity(@RequestBody UpstartEntity entity) {
        entityRepository.save(entity);
        return new ServiceResponse();
    }

    @DeleteMapping(path = "entity")
    public @ResponseBody
    ServiceResponse deleteEntity(@RequestBody UpstartEntity entity) {
        entityRepository.delete(entity);
        return new ServiceResponse();
    }

    @GetMapping(path = "entity")
    public @ResponseBody
    UpstartEntityResponse getEntity(@QueryParam("id") Integer id) {
        UpstartEntityResponse response = new UpstartEntityResponse();
        Optional<UpstartEntity> entity = entityRepository.findById(id);
        entity.ifPresent(response::setEntity);
        return response;
    }

    @GetMapping(path = "entity/all")
    public @ResponseBody
    UpstartEntityListResponse getAllEntities() {
        UpstartEntityListResponse response = new UpstartEntityListResponse();
        Iterable<UpstartEntity> entities = entityRepository.findAll();
        response.setEntities(entities);
        return response;
    }
}
