package com.social.upstart.controller;

import com.social.upstart.domain.response.version.VersionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

/**
 * Controller that handles the Versioning endpoint.  Can be utilized for a health check as well as returning some
 * instance metadata -- including but not limited to semantic version, and active spring-boot profiles.
 */
@RestController
@RequestMapping(path = "version")
public class VersionController extends BaseController {
    Environment environment;

    @Autowired
    public VersionController(Environment environment) {
        super(VersionController.class);
        this.environment = environment;
    }

    @GetMapping()
    public VersionResponse getVersion() throws Exception {
        VersionResponse ret = new VersionResponse();
        ret.setActiveProfiles(Arrays.asList(environment.getActiveProfiles()));
        return ret;
    }
}
