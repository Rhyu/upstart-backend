package com.social.upstart.domain.request;

import com.social.upstart.domain.upstart.UpstartEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class UpstartEntityRequest extends ServiceRequest {
    private UpstartEntity entity;
}
