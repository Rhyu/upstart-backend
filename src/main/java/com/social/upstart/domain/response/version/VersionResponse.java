package com.social.upstart.domain.response.version;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.social.upstart.domain.response.ServiceResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VersionResponse extends ServiceResponse {
    private String major;
    private String minor;
    private String rc;
    private List<String> activeProfiles;
}
