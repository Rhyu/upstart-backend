package com.social.upstart.domain.response.upstart;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.social.upstart.domain.response.ServiceResponse;
import com.social.upstart.domain.upstart.UpstartEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpstartEntityListResponse extends ServiceResponse {
    private Iterable<UpstartEntity> entities;

}
