package com.social.upstart.domain.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceResponse {
    /**
     * Service Response Types
     **/
    public static final String SUCCESS = "SUCCESS";
    public static final String ERROR = "ERROR";

    private String status = SUCCESS;
    private String errorCode;
    private List<String> errorMessages;

    /**
     * Helper Methods for managing errorMessage list
     **/
    public void addErrorMessage(String errorMessage) {
        if (errorMessages == null) {
            errorMessages = new ArrayList<>();
        }
        errorMessages.add(errorMessage);
    }

    public void addErrorMessages(String... errorMessageVarArgs) {
        if (errorMessages == null) {
            errorMessages = new ArrayList<>();
        }
        errorMessages.addAll(Arrays.asList(errorMessageVarArgs));
    }

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
