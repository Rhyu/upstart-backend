package com.social.upstart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpstartApplication {

    public static void main(String[] args) {
        SpringApplication.run(UpstartApplication.class, args);
    }

}
