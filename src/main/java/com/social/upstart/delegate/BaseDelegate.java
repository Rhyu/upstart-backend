package com.social.upstart.delegate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BaseDelegate {
    protected Logger LOGGER;

    public BaseDelegate(Class rootClass) {
        LOGGER = LogManager.getLogger(rootClass);
    }
}
