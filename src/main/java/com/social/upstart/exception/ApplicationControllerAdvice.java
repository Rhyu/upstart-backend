package com.social.upstart.exception;

import com.social.upstart.domain.response.ServiceResponse;
import com.social.upstart.exception.exceptions.ExampleException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ApplicationControllerAdvice extends ResponseEntityExceptionHandler {
    private final static Logger LOGGER = LogManager.getLogger(ApplicationControllerAdvice.class);

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ServiceResponse> handleGenericException(Exception ex) {
        GenericExceptionHandler handler = new GenericExceptionHandler();
        return handler.handleException(ex);
    }

    @ExceptionHandler(ExampleException.class)
    public ResponseEntity<ServiceResponse> handleExampleException(ExampleException ex) {
        ExampleExceptionHandler handler = new ExampleExceptionHandler();
        return handler.handleException(ex);
    }
}
