package com.social.upstart.exception;

import com.social.upstart.domain.response.ServiceResponse;
import org.springframework.http.ResponseEntity;

import javax.ws.rs.core.Response;
import java.util.Arrays;

public class GenericExceptionHandler extends BaseExceptionHandler<Exception> {
    public GenericExceptionHandler() {
        super(ExampleExceptionHandler.class);
    }

    public ResponseEntity<ServiceResponse> handleException(Exception e) {
        e.printStackTrace();
        response.setErrorMessages(Arrays.asList(e.getMessage()));
        return build();
    }
}