package com.social.upstart.exception.exceptions;

import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Supplier;
import java.util.stream.Stream;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

import static java.util.Collections.unmodifiableSortedMap;

@Getter
public enum ExampleExceptionType {
    //@formatter:off
    GENERIC_ERROR(0, "Generic Error. [1]", HttpStatus.OK);
    //@formatter:on

    final int errorCode;
    final String description;
    final HttpStatus statusCode;
    private static final Supplier<SortedMap<String, ExampleExceptionType>> sortedMapSupplier = () -> new TreeMap<>((s1, s2) -> StringUtils.trimToEmpty(s1).compareToIgnoreCase(StringUtils.trimToEmpty(s2)));
    static final SortedMap<Integer, ExampleExceptionType> typeByErrorCode = unmodifiableSortedMap(Stream.of(values()).collect(TreeMap::new, (m, v) -> m.put(v.errorCode, v), TreeMap::putAll));
    static final SortedMap<String, ExampleExceptionType> typeByName = unmodifiableSortedMap(Stream.of(values()).collect(sortedMapSupplier, (m, v) -> m.put(v.description, v), SortedMap::putAll));

    ExampleExceptionType(final int errorCode, final String description, final HttpStatus statusCode) {
        this.errorCode = errorCode;
        this.description = description;
        this.statusCode = statusCode;
    }

    public static Optional<ExampleExceptionType> fromErrorCode(final int errorCode) {
        return Optional.ofNullable(typeByErrorCode.get(errorCode));
    }

    public static Optional<ExampleExceptionType> fromName(final String name) {
        return Optional.ofNullable(typeByName.get(name));
    }

    @Override
    public String toString() {
        return String.format("%s(%s)", super.toString(), description);
    }
}
