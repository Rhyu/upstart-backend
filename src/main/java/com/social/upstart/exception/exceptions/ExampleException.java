package com.social.upstart.exception.exceptions;

public class ExampleException extends Exception {
    final ExampleExceptionType type;

    public ExampleException(final ExampleExceptionType type) {
        super(type.description);
        this.type = type;
    }

    public int getErrorCode() {
        return type.errorCode;
    }

    public ExampleExceptionType getType() {
        return type;
    }

    @Override
    public String getMessage() {
        return type.description;
    }
}
