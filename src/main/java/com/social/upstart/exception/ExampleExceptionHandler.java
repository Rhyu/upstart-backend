package com.social.upstart.exception;

import com.social.upstart.domain.response.ServiceResponse;
import com.social.upstart.exception.exceptions.ExampleException;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;

/**
 * Example Customer Exception Handler to be handle exceptions of type {@link com.social.resttemplate.exception.exceptions.ExampleException}
 */
public class ExampleExceptionHandler extends BaseExceptionHandler<ExampleException> {
    public ExampleExceptionHandler() {
        super(ExampleExceptionHandler.class);
    }

    public ResponseEntity<ServiceResponse> handleException(ExampleException e) {
        e.printStackTrace();
        status = e.getType().getStatusCode();
        response.setErrorCode(Integer.toString(e.getErrorCode()));
        response.setErrorMessages(Arrays.asList(e.getMessage()));
        return build();
    }
}
