package com.social.upstart.exception;

import com.social.upstart.domain.response.ServiceResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class BaseExceptionHandler<C extends Throwable> {
    public static Logger LOGGER;
    public HttpStatus status = HttpStatus.OK;
    public ServiceResponse response = new ServiceResponse().setStatus(ServiceResponse.ERROR);

    public BaseExceptionHandler(Class handler) {
        LOGGER = LogManager.getLogger(handler);
    }

    public ResponseEntity<ServiceResponse> build() {
        return new ResponseEntity<>(response, status);
    }
}
