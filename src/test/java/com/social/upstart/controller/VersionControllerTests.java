package com.social.upstart.controller;

import com.social.upstart.domain.response.ServiceResponse;
import com.social.upstart.domain.response.version.VersionResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.google.common.truth.Truth.assertThat;

@SpringBootTest(classes = {VersionController.class})
public class VersionControllerTests extends ControllerTestConstants {
    private static final Logger LOGGER = LogManager.getLogger(VersionControllerTests.class);
    @Autowired
    VersionController controller;

    @Before
    public void setup() {

    }

    @After
    public void testDown() {

    }


    @Test
    public void testVersionResponse() throws Exception {
        VersionResponse response = controller.getVersion();
        LOGGER.info(response);
        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(ServiceResponse.SUCCESS);
        assertThat(response.getActiveProfiles()).isEmpty();
    }
}
